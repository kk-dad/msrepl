package com.example.demo.service;

import com.example.demo.entity.User;

public interface UserService {
    public User getById(int id);

    public void save(User user);
}
