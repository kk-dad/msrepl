package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping("/get/{id}")
    public User queryById(@PathVariable("id") int id) {
        return userService.getById(id);
    }

    @RequestMapping("/save")
    public void addDept(User user) {
        userService.save(user);
    }
}
