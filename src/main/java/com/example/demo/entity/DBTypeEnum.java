package com.example.demo.entity;

/**
 * 定义一个枚举来代表这三个数据源
 */

public enum DBTypeEnum {
    MASTER, SLAVE1;
}